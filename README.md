# Stuffs

Various things I've drafted up for other various things, peruse to your hearts content.

## Things of Stuffs

```
├── docker_images
│   └── centos:7-lamp   # A CentOS 7 LAMP Image
├── html
│   ├── 4lambda         # Website stuff
├── linux
│   ├── etc             # /etc files
│   ├── rusty           # /rusty config files
│   ├── usr             # /usr files
│   └── var             # /var files
├── school
│   ├── 4061-repo       # Operating Systems
│   ├── 4131-repo       # Web Applications
│   ├── 4211-repo       # Networking
│   └── 4707-repo       # Databases
├── src
│   ├── c               # C\C++ & OpenACC
│   ├── f90             # Fortran90
│   ├── perl            # Perl
│   ├── py              # Python
│   └── ruby            # Ruby
└── widgets             # Übersicht Configurations
```
<p align="center">
    <img src="http://i.imgur.com/GvVk095.gif" alt="banana dolphin")
</p>
